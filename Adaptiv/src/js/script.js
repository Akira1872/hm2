
const onSvg = document.querySelector('.on');
const ofSvg = document.querySelector('.of');
const pseudoElement = document.querySelector('.svg-on');
const burger = document.querySelector('.burger');
let isSvgVisible = true;

onSvg.addEventListener('click', () => {
  if (isSvgVisible) {
    pseudoElement.style.display = 'none';
    burger.style.display = 'flex';
  } else {
    pseudoElement.style.display = 'block';
    burger.style.display = 'none';
  }
  isSvgVisible = !isSvgVisible;
});

ofSvg.addEventListener('click', () => {
  pseudoElement.style.display = 'block';
  burger.style.display = 'none';
  isSvgVisible = true;
});
